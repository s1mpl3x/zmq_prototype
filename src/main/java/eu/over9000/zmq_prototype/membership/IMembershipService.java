package eu.over9000.zmq_prototype.membership;

import java.io.IOException;
import java.net.InetAddress;
import java.util.function.Consumer;

import eu.over9000.zmq_prototype.model.Node;

public interface IMembershipService {
	
	void start(Node node) throws IOException;
	
	void stop() throws IOException;

	void registerMembershipEventListener(Consumer<MembershipEvent> callback);



}
