package eu.over9000.zmq_prototype.membership;

import java.io.Serializable;

import eu.over9000.zmq_prototype.model.Node;

public class MembershipMessage implements Serializable {
	
	private static final long serialVersionUID = -1874067358829135969L;

	private final Node node;
	private final String responseAddress;
	private boolean isLeaving;
	
	public MembershipMessage(final Node node, final String responseAddress, final boolean isLeaving) {
		this.node = node;
		this.responseAddress = responseAddress;
		this.isLeaving = isLeaving;
	}
	
	public Node getNode() {
		return this.node;
	}
	
	public String getResponseAddress() {
		return this.responseAddress;
	}
	
	public boolean isLeaving() {
		return this.isLeaving;
	}
	
	public void setLeaving(final boolean isLeaving) {
		this.isLeaving = isLeaving;
	}
	
}
