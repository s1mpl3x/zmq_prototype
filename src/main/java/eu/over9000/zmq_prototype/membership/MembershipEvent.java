package eu.over9000.zmq_prototype.membership;

import java.time.Instant;

import eu.over9000.zmq_prototype.model.Node;

public class MembershipEvent {
	public enum EventType {
		JOIN, LEAVE, TIMEOUT;
	};
	
	private final Node node;
	private final EventType type;
	private final Instant timestamp;
	
	public MembershipEvent(final Node node, final EventType type) {
		this.node = node;
		this.type = type;
		this.timestamp = Instant.now();
	}
	
	public Node getNode() {
		return this.node;
	}
	
	public EventType getType() {
		return this.type;
	}
	
	public Instant getTimestamp() {
		return this.timestamp;
	}
	
}
