package eu.over9000.zmq_prototype.membership;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

import net.jodah.expiringmap.ExpiringMap;
import net.jodah.expiringmap.ExpiringMap.ExpirationListener;
import eu.over9000.zmq_prototype.membership.MembershipEvent.EventType;
import eu.over9000.zmq_prototype.model.Node;

public class DefaultMembershipService implements IMembershipService {
	
	private final ExpirationListener<UUID, Node> listener = new ExpirationListener<UUID, Node>() {

		@Override
		public void expired(final UUID key, final Node value) {
			callback.accept(new MembershipEvent(value, EventType.TIMEOUT));
		}
	};

	private Map<UUID, Node> knownNodes;
	private static final int REQUEST_PORT = 19791;
	private static final InetAddress MULTICAST_GROUP;
	
	static {
		InetAddress tmpAddress = null;
		try {
			tmpAddress = InetAddress.getByName("239.255.255.250");
		} catch (final UnknownHostException e) {
			e.printStackTrace();
		}
		MULTICAST_GROUP = tmpAddress;
	}
	
	private static final int TIMEOUT = 5;
	private static final int HEARTBEAT = 1;
	
	private AtomicBoolean alive;

	private DatagramSocket membershipSocket;
	private MembershipMessage membershipMessage;
	
	private final Object sendLock = new Object();
	
	private Consumer<MembershipEvent> callback;
	
	private Thread sendThread;
	private Thread receiverThread;

	@Override
	public void start(final Node node) throws IOException {
		try {
			
			knownNodes = ExpiringMap.builder().expiration(DefaultMembershipService.TIMEOUT, TimeUnit.SECONDS).expirationListener(listener).build();

			membershipSocket = new DatagramSocket(null);
			membershipSocket.setReuseAddress(true);
			membershipSocket.bind(new InetSocketAddress(REQUEST_PORT));

			membershipMessage = new MembershipMessage(node, membershipSocket.getLocalAddress().toString(), false);

			sendThread = new Thread(new MembershipSender(), "membership send thread");
			receiverThread = new Thread(new MembershipReceiver(), "membership receiver thread");

			alive = new AtomicBoolean(true);
			
			sendThread.start();
			receiverThread.start();
			
			System.out.println("membership service startup done");
			
		} catch (final SocketException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void stop() throws IOException {
		alive.set(false);
		sendThread.interrupt();
		receiverThread.interrupt();
		// try {
		// this.membershipMessage.setLeaving(true);
		// final byte[] msg = this.buildMsg();
		//
		// synchronized (this.sendLock) {
		// final DatagramPacket packet = new DatagramPacket(msg, 0, msg.length, InetAddress.getByName("255.255.255.255"),
		// DefaultMembershipService.REQUEST_PORT);
		// this.membershipSocket.send(packet);
		// }
		//
		// } catch (final IOException e) {
		// e.printStackTrace();
		// }

		// this.membershipSocket.leaveGroup(MULTICAST_GROUP);
		membershipSocket.close();

	}

	private byte[] buildMsg() throws IOException {
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
		objectOutputStream.writeObject(membershipMessage);
		objectOutputStream.flush();
		return byteArrayOutputStream.toByteArray();
	}
	
	@Override
	public void registerMembershipEventListener(final Consumer<MembershipEvent> callback) {
		this.callback = callback;
	}
	
	private class MembershipReceiver implements Runnable {

		@Override
		public void run() {
			
			while (alive.get()) {
				try {
					final byte[] buffer = new byte[1024 * 4];
					final DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
					membershipSocket.receive(packet);

					final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(buffer, packet.getOffset(), packet.getLength());
					final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
					
					final MembershipMessage msg = (MembershipMessage) objectInputStream.readObject();

					// System.out.println("received " + msg);
					
					if (knownNodes.containsKey(msg.getNode().getId())) {
						// already known
						if (msg.isLeaving()) {
							// remove
							knownNodes.remove(msg.getNode().getId());
							callback.accept(new MembershipEvent(msg.getNode(), EventType.LEAVE));
						} else {
							// refresh
							knownNodes.remove(msg.getNode().getId());
							knownNodes.put(msg.getNode().getId(), msg.getNode());
						}
					} else {
						// unknown
						if (!msg.isLeaving()) {
							// add
							knownNodes.put(msg.getNode().getId(), msg.getNode());
							callback.accept(new MembershipEvent(msg.getNode(), EventType.JOIN));
						}
					}
					
				} catch (final IOException | ClassNotFoundException e) {
					if (alive.get()) {
						e.printStackTrace();
					}
					return;
				}
			}
			
		}
	}
	
	private class MembershipSender implements Runnable {
		
		@Override
		public void run() {
			try {
				final byte[] msg = buildMsg();

				while (alive.get()) {
					
					synchronized (sendLock) {
						final DatagramPacket packet = new DatagramPacket(msg, 0, msg.length, InetAddress.getByName("255.255.255.255"), DefaultMembershipService.REQUEST_PORT);
						
						membershipSocket.send(packet);
					}

					try {
						Thread.currentThread();
						Thread.sleep(DefaultMembershipService.HEARTBEAT * 1000);
					} catch (final InterruptedException e) {
						if (alive.get()) {
							e.printStackTrace();
						}
						return;
					}
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}

		}

	}

}
