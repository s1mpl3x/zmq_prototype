package eu.over9000.zmq_prototype.model;

import java.io.Serializable;
import java.util.UUID;

public class Node implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 6392955750888779868L;
	

	private final UUID id;
	private final String pubAddress;
	private final String routerAddress;

	public Node(final String pubAddress, final String routerAddress) {
		this.id = UUID.randomUUID();
		this.pubAddress = pubAddress;
		this.routerAddress = routerAddress;
		System.out.println("my id: " + id);
	}
	
	public Node(String uuid, final String pubAddress, final String routerAddress) {
		this.id = UUID.fromString(uuid);
		this.pubAddress = pubAddress;
		this.routerAddress = routerAddress;
		System.out.println("my id: " + id);
	}

	public UUID getId() {
		return this.id;
	}
	
	public String getPubAddress() {
		return this.pubAddress;
	}
	
	public String getRouterAddress() {
		return this.routerAddress;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	
	
	
}