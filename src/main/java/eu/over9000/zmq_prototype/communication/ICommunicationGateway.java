package eu.over9000.zmq_prototype.communication;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import eu.over9000.zmq_prototype.membership.MembershipEvent;
import eu.over9000.zmq_prototype.model.Node;

public interface ICommunicationGateway {

	void start() throws IOException;
	
	void stop() throws IOException;
	
	List<Node> getNodes();

	void publish(String topic, byte[] message);
	
	void subscribe(String topic);

	void unsubscribe(String topic);
	
	Future<byte[]> sendRequest(Node node, byte[] message) throws CommunicationGatewayException;
	
	void registerRequestHandler(Function<byte[], byte[]> requestHandler);
	
	void registerMembershipChangeListener(Consumer<MembershipEvent> callback);
	
	void registerSubscriptionCallback(BiConsumer<String, byte[]> callback);
}
