package eu.over9000.zmq_prototype.communication;

public class CommunicationGatewayException extends Exception {
	
	public CommunicationGatewayException(final Exception e) {
		super(e);
	}

	/**
	 *
	 */
	private static final long serialVersionUID = 1361191742713963926L;

}
