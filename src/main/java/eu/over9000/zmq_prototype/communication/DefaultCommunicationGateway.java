package eu.over9000.zmq_prototype.communication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Context;
import org.zeromq.ZMQ.Poller;
import org.zeromq.ZMQ.Socket;
import org.zeromq.ZMsg;

import com.google.common.util.concurrent.SettableFuture;

import eu.over9000.zmq_prototype.membership.DefaultMembershipService;
import eu.over9000.zmq_prototype.membership.IMembershipService;
import eu.over9000.zmq_prototype.membership.MembershipEvent;
import eu.over9000.zmq_prototype.model.Node;

public class DefaultCommunicationGateway implements ICommunicationGateway {
	
	private Poller poller;

	private AtomicBoolean alive;

	private final Node identity;

	private Context context;

	private Socket pubSocket;
	private Socket subSocket;
	private Socket routerSocket;
	private Socket reqSocket;

	private Consumer<MembershipEvent> membershipChangeListener;
	private Function<byte[], byte[]> requestHandler;

	private List<Node> nodes;

	private BiConsumer<String, byte[]> subCallback;

	private Thread pollerThread;

	private IMembershipService membershipService;

	private final AtomicInteger maxId = new AtomicInteger();

	private final Map<String, SettableFuture<byte[]>> responseFutures = new ConcurrentHashMap<>(10_000_000);

	private final Queue<ZMsg> outstandingRequests = new ConcurrentLinkedQueue<>();
	private final Queue<ZMsg> outstandingPublishs = new ConcurrentLinkedQueue<>();
	private final Queue<MembershipEvent> outstandingMembershitEvents = new ConcurrentLinkedQueue<>();
	private final Queue<SubscriptionChange> outstandingSubChanges = new ConcurrentLinkedQueue<>();

	public DefaultCommunicationGateway(final Node identity) {
		this.identity = identity;
	}

	@Override
	public void start() throws IOException {

		nodes = new ArrayList<>();

		context = ZMQ.context(1);
		pubSocket = context.socket(ZMQ.PUB);
		pubSocket.bind(identity.getPubAddress());

		routerSocket = context.socket(ZMQ.ROUTER);
		routerSocket.setIdentity(identity.getId().toString().getBytes());
		routerSocket.bind(identity.getRouterAddress());
		routerSocket.setHWM(10_000_000);
		routerSocket.setRouterMandatory(true);

		reqSocket = context.socket(ZMQ.ROUTER);
		// reqSocket.setIdentity("DANK".getBytes());
		reqSocket.setHWM(10_000_000);
		reqSocket.setRouterMandatory(true);
		
		subSocket = context.socket(ZMQ.SUB);

		alive = new AtomicBoolean(true);

		poller = new Poller(3);
		poller.register(routerSocket, Poller.POLLIN);
		poller.register(subSocket, Poller.POLLIN);
		poller.register(reqSocket, Poller.POLLIN);

		pollerThread = new Thread(new SocketPoller(), "Incoming Message Handler");
		pollerThread.start();

		membershipService = new DefaultMembershipService();
		membershipService.registerMembershipEventListener(event -> outstandingMembershitEvents.add(event));
		membershipService.start(identity);
		System.out.println("com gateway start done");
	}
	
	private void handleMembershipEventForSockets(final MembershipEvent event) {
		final Node node = event.getNode();
		switch (event.getType()) {
			case JOIN:
				subSocket.connect(node.getPubAddress());
				reqSocket.connect(node.getRouterAddress());
				nodes.add(event.getNode());
				break;

			case LEAVE:
			case TIMEOUT:
				nodes.remove(event.getNode());
				subSocket.disconnect(node.getPubAddress());
				reqSocket.disconnect(node.getRouterAddress());
				break;

			default:
				throw new IllegalStateException();
		}
		System.out.println("handled node join/leave");
		membershipChangeListener.accept(event);
	}

	@Override
	public void stop() throws IOException {
		membershipService.stop();

		try {
			Thread.sleep(1000);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}

		alive.set(false);
		pubSocket.close();
		subSocket.close();
		reqSocket.close();
		routerSocket.close();
		context.term();

		pollerThread.interrupt();

	}

	@Override
	public List<Node> getNodes() {
		return Collections.unmodifiableList(nodes);
	}

	@Override
	public void publish(final String topic, final byte[] message) {
		Objects.requireNonNull(topic);
		final ZMsg msg = new ZMsg();
		msg.add(topic.getBytes());
		msg.add(message);
		outstandingPublishs.add(msg);
	}

	@Override
	public void subscribe(final String topic) {
		Objects.requireNonNull(topic);
		outstandingSubChanges.add(new SubscriptionChange(topic.getBytes(), true));
	}

	@Override
	public void unsubscribe(final String topic) {
		Objects.requireNonNull(topic);
		outstandingSubChanges.add(new SubscriptionChange(topic.getBytes(), false));
	}

	@Override
	public Future<byte[]> sendRequest(final Node node, final byte[] message) throws CommunicationGatewayException {

		final Integer id = maxId.incrementAndGet();
		final SettableFuture<byte[]> future = SettableFuture.create();

		responseFutures.put(id.toString(), future);

		final ZMsg msg = new ZMsg();

		msg.add(node.getId().toString().getBytes());
		msg.add("");
		msg.add(id.toString());
		msg.add(message);

		outstandingRequests.add(msg);

		return future;
	}

	@Override
	public void registerRequestHandler(final Function<byte[], byte[]> function) {
		Objects.requireNonNull(function);
		requestHandler = function;
	}

	@Override
	public void registerSubscriptionCallback(final BiConsumer<String, byte[]> callback) {
		Objects.requireNonNull(callback);
		subCallback = callback;
	}

	@Override
	public void registerMembershipChangeListener(final Consumer<MembershipEvent> callback) {
		Objects.requireNonNull(callback);
		membershipChangeListener = callback;
	}

	private class SocketPoller implements Runnable {

		@Override
		public void run() {
			while (alive.get()) {

				poller.poll(10);

				handleChanges();

				if (poller.pollin(0)) { // handle router
					handleRouterIn();
				}
				if (poller.pollin(1)) { // handle sub
					handleSubIn();
				}
				if (poller.pollin(2)) { // handle responses
					handleResponseIn();
				}
				// send req
				sendRequests();

				// send pub
				sendPublishs();
				
			}
		}

		private void handleChanges() {
			while (!outstandingMembershitEvents.isEmpty()) { // react on membership changes
				handleMembershipEventForSockets(outstandingMembershitEvents.remove());
			}
			while (!outstandingSubChanges.isEmpty()) { // apply sub changes
				handleSubChanges(outstandingSubChanges.remove());
			}
		}

		private void handleSubChanges(final SubscriptionChange subChange) {
			if (subChange.add) {
				subSocket.subscribe(subChange.topic);
			} else {
				subSocket.unsubscribe(subChange.topic);
			}
		}
		
		private void sendPublishs() {
			while (!outstandingPublishs.isEmpty()) {
				final ZMsg msg = outstandingPublishs.remove();
				msg.send(pubSocket);
			}
		}

		private void sendRequests() {
			while (!outstandingRequests.isEmpty()) {
				final ZMsg msg = outstandingRequests.remove();
				msg.send(reqSocket);
			}
		}

		private void handleResponseIn() {
			ZMsg msg;

			msg = ZMsg.recvMsg(reqSocket);

			final ZFrame[] frames = msg.toArray(new ZFrame[0]);

			final String id = frames[frames.length - 2].toString();
			final byte[] content = frames[frames.length - 1].getData();

			final SettableFuture<byte[]> future = responseFutures.remove(id);

			if (future != null) {
				future.set(content);
			} else {
				System.out.println("UNKOWN ID: " + id);
			}

		}

		private void handleSubIn() {
			final ZMsg msg = ZMsg.recvMsg(subSocket);

			final String topic = msg.getFirst().toString();
			final byte[] content = msg.getLast().getData();

			subCallback.accept(topic, content);
		}

		private void handleRouterIn() {

			final ZMsg msg = ZMsg.recvMsg(routerSocket);

			final ZFrame[] frames = msg.toArray(new ZFrame[0]);

			final byte[] addr = frames[0].getData();
			final byte[] id = frames[frames.length - 2].getData();
			final byte[] content = frames[frames.length - 1].getData();

			final byte[] response = requestHandler.apply(content);

			routerSocket.sendMore(addr);
			routerSocket.sendMore("");
			routerSocket.sendMore(id);
			routerSocket.send(response);
		}
	}

	private class SubscriptionChange {
		public final byte[] topic;
		public final boolean add;

		public SubscriptionChange(final byte[] topic, final boolean add) {
			this.topic = topic;
			this.add = add;
		}
	}

}
