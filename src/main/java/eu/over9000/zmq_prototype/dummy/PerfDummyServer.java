package eu.over9000.zmq_prototype.dummy;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import eu.over9000.zmq_prototype.communication.CommunicationGatewayException;
import eu.over9000.zmq_prototype.communication.DefaultCommunicationGateway;
import eu.over9000.zmq_prototype.communication.ICommunicationGateway;
import eu.over9000.zmq_prototype.model.Node;

public class PerfDummyServer {

	private final ICommunicationGateway communicationGateway;
	
	private final Node controller = new Node("791b9285-617b-4a0c-b1d2-76180c7f48a3", "tcp://192.168.0.4:9393", "tcp://192.168.0.4:9494");

	private int server = 0;

	public PerfDummyServer() {
		communicationGateway = new DefaultCommunicationGateway(controller);
		communicationGateway.registerMembershipChangeListener(event -> {
			System.out.println(event.getNode().getId() + " -> " + event.getType());
		});
		communicationGateway.registerRequestHandler(request -> {
			final String requestS = new String(request);
			server++;
			return ("RESPONSE TO " + requestS).getBytes();
		});
		communicationGateway.registerSubscriptionCallback((topic, message) -> {
			System.out.println(">>> received sub msg on topic '" + topic + "': " + new String(message));
		});
	}
	
	private void runTest() throws InterruptedException, CommunicationGatewayException, IOException, ExecutionException {
		communicationGateway.start();

		while (true) {
			Thread.sleep(5000);

			communicationGateway.publish("dank", "memes".getBytes());
			System.out.println("s: " + server);
		}
		
		// Thread.sleep(500);
		//
		// this.communicationGateway.subscribe("a");
		//
		// Thread.sleep(500);
		//
		// this.communicationGateway.publish("ayy", "kek".getBytes());
		//
		// int cnt_out = 0;
		// int cnt_in = 0;
		//
		// final List<Future<byte[]>> q = new LinkedList<>();
		//
		//
		// Node other = null;
		// outer: while (true) {
		// for (Node node : communicationGateway.getNodes()) {
		// if (!node.equals(this.controller)) {
		// other = node;
		// break outer;
		// }
		// }
		//
		// Thread.sleep(100);
		//
		// }
		//
		// final long start = System.currentTimeMillis();
		// while (System.currentTimeMillis() - start < 10000) {
		//
		// q.add(this.communicationGateway.sendRequest(other, "wasd".getBytes()));
		// cnt_out++;
		// }
		//
		// for (int i = 0; i < 20; i++) {
		//
		// for (final Iterator<Future<byte[]>> iterator = q.iterator(); iterator.hasNext();) {
		// final Future<byte[]> future = iterator.next();
		// if (future.isDone()) {
		// cnt_in++;
		// iterator.remove();
		// }
		// }
		//
		// System.out.println(i + ":");
		// System.out.println("clt_out		" + cnt_out);
		// System.out.println("clt_in		" + cnt_in);
		// System.out.println("srv_in		" + this.server);
		//
		// if (q.isEmpty()) {
		// break;
		// }
		//
		// Thread.sleep(1000);
		// }
		//
		// this.communicationGateway.stop();
		//
		// // explicit exit as the expiring-map prevents the jvm from terminating..
		// System.exit(0);
	}
	
	public static void main(final String[] args) throws InterruptedException, CommunicationGatewayException, IOException, ExecutionException {
		final PerfDummyServer c = new PerfDummyServer();
		c.runTest();
	}
}
