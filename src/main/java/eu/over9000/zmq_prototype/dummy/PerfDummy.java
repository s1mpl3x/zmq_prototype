package eu.over9000.zmq_prototype.dummy;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Future;

import eu.over9000.zmq_prototype.communication.CommunicationGatewayException;
import eu.over9000.zmq_prototype.communication.DefaultCommunicationGateway;
import eu.over9000.zmq_prototype.communication.ICommunicationGateway;
import eu.over9000.zmq_prototype.model.Node;

public class PerfDummy {
	
	private final ICommunicationGateway communicationGateway;
	private final Node controller = new Node("tcp://192.168.0.4:8393", "tcp://192.168.0.4:8494");
	
	private int server = 0;
	
	public PerfDummy() {
		communicationGateway = new DefaultCommunicationGateway(controller);
		communicationGateway.registerMembershipChangeListener(event -> {
			System.out.println(event.getNode().getId() + " -> " + event.getType());
		});
		communicationGateway.registerRequestHandler(request -> {
			final String requestS = new String(request);
			server++;
			return ("RESPONSE TO " + requestS).getBytes();
		});
		communicationGateway.registerSubscriptionCallback((topic, message) -> {
			System.out.println(">>> received sub msg on topic '" + topic + "': " + new String(message));
		});
	}

	private void runTest() throws InterruptedException, CommunicationGatewayException, IOException {
		communicationGateway.start();

		Thread.sleep(1000);

		communicationGateway.subscribe("dank");

		int cnt_out = 0;
		int cnt_in = 0;
		
		final List<Future<byte[]>> q = new LinkedList<>();

		Node other = null;
		outer: while (true) {
			for (final Node node : communicationGateway.getNodes()) {
				if (node.getId().toString().equals("791b9285-617b-4a0c-b1d2-76180c7f48a3")) {
					other = node;
					break outer;
				}
			}
			
			Thread.sleep(100);
		}

		Thread.sleep(2000);
		System.out.println("found other node, sending requests..");
		final long start = System.currentTimeMillis();
		while (System.currentTimeMillis() - start < 10000) {

			q.add(communicationGateway.sendRequest(other, "wasd".getBytes()));
			cnt_out++;
		}
		System.out.println("done");

		for (int i = 0; i < 20; i++) {
			
			for (final Iterator<Future<byte[]>> iterator = q.iterator(); iterator.hasNext();) {
				final Future<byte[]> future = iterator.next();
				if (future.isDone()) {
					cnt_in++;
					iterator.remove();
				}
			}

			System.out.println(i + ":");
			System.out.println("clt_out		" + cnt_out);
			System.out.println("clt_in		" + cnt_in);
			// System.out.println("srv_in		" + server);

			if (q.isEmpty()) {
				break;
			}

			Thread.sleep(1000);
		}

		communicationGateway.stop();

		// explicit exit as the expiring-map prevents the jvm from terminating..
		System.exit(0);
	}

	public static void main(final String[] args) throws InterruptedException, CommunicationGatewayException, IOException {
		final PerfDummy c = new PerfDummy();
		c.runTest();
	}
}
